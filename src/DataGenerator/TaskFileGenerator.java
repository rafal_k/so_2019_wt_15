package DataGenerator;

import java.util.ArrayList;
import java.util.Random;

public class TaskFileGenerator {
    private int time;
    private int lastId;
    private int NumberOfTasks;
    private Random random;
    private Random timeRandom;

    private final static double deviation = 4;
    private final static int mean = 20;

    private final static double timeDeviation = 0.5;
    private final static int timeMean = 3;

    public TaskFileGenerator(int number) {
        this.time = 0;
        this.NumberOfTasks = number;
        this.random = new Random();
        this.timeRandom = new Random();
    }

    public ArrayList<Task> generateArrayOfTasks() {

        ArrayList<Task> list = new ArrayList<>();

        while(list.size() < this.NumberOfTasks) {

            int howMany = this.timeNextGaussian();

            for (int i = 0; i < howMany && list.size() < this.NumberOfTasks; i++) {
                list.add(this.newTask(list.size()));
            }

            this.time++;
        }

        return list;
    }

    private Task newTask(int id) {
        //Task temp = new Task(1, id, this.time);
        Task temp = new Task(this.nextGaussian(), id, this.time);

        return temp;
    }

    private int nextGaussian(){
        return (int)(random.nextGaussian() * TaskFileGenerator.deviation + TaskFileGenerator.mean);
    }

    private int timeNextGaussian(){
        return (int)(timeRandom.nextGaussian() * TaskFileGenerator.timeDeviation + TaskFileGenerator.timeMean);
    }
}