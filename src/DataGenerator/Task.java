package DataGenerator;

import java.io.Serializable;
import java.util.Comparator;

import main.Main;

public class Task implements Serializable{

    private long executionTime;

    private long workStartTime;
    private long creationTime;
    private long waitingTime;
    private long taskID;
    private long finishTime;

    public Task (long executionTime, long taskID, long creationTime) {
        this.executionTime = executionTime;
        this.taskID = taskID;
        this.creationTime = creationTime;
        this.waitingTime = 0;
        this.finishTime = 0;
        this.workStartTime = 0;
    }

    //akcesory

    public long getExecuteTime() {
        return this.executionTime;
    }

    public long getTaskID() {
        return this.taskID;
    }

    public long getCreationTime() {
        return this.creationTime;
    }

    public boolean isFinished() { return this.finishTime > 0; }

    public void setFinished() {
        this.log("finished");
        this.finishTime = Main.getTime();
    }

    public void setStarted() {
        if (this.workStartTime == 0) {
            this.log("started");
            this.workStartTime = Main.getTime();
        }
    }

    public long getWaitingTime() { return this.waitingTime; }

    public void incrementWaitingTime() { this.waitingTime++; }

    public boolean isWaiting() {
        return this.waitingTime > 0 && !this.isFinished();
    }

    public void log(String message) {
        Main.logger.log("TASK", String.format("(Task %d) %s", this.taskID, message));
    }

    public static Comparator<Task> priorityComparator() {
        return new TaskPriorityComparator();
    }
}