package DataGenerator;

import main.Main;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Generator {
    private int numberOfTasks;
    private int taskCounter;

    private ArrayList<Task> ourList;
    private ArrayList<Task> publicList;

    private FileOperations fileOperations;

    public Generator(int numberOfTasks) {
        this.numberOfTasks = numberOfTasks;
        this.taskCounter = 0;

        this.publicList = new ArrayList<>();                //uwtórz tablice prywatna i publiczna

        this.fileOperations = new FileOperations();         //utwórz obiekt do wczytywania i zapisywania
    }

    public boolean allTasksFinishedOrWaiting() {
        for (Task task : this.ourList) {
            if (!task.isFinished() && !task.isWaiting()) return false;
        }
        return true;
    }

    public boolean allTasksFinished() {
        for (Task task : this.ourList) {
            if (!task.isFinished()) return false;
        }
        return true;
    }

    public ArrayList<Task> getNewTasks() {
        if (this.allTasksFinishedOrWaiting())
            return new ArrayList<>();

        ArrayList<Task> tasksToAdd = new ArrayList<>(this.ourList.stream()
                .filter(task -> Main.getTime() >= task.getCreationTime())
                .filter(task -> !task.isFinished())
                .filter(task -> !task.isWaiting())
                .collect(Collectors.toList()));

        this.taskCounter += tasksToAdd.size();
        return tasksToAdd;
    }

    public void generateData() {
        TaskFileGenerator generator = new TaskFileGenerator(this.numberOfTasks);
        this.fileOperations.saveArray(generator.generateArrayOfTasks());        //zapisz liste procesow
    }

    public void loadData() {
        this.ourList = this.fileOperations.loadArray();                         //wczytaj liste procesów
    }

    public ArrayList<Task> getTasksArrayList() {
        return this.ourList;
    }

}