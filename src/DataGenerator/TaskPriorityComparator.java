package DataGenerator;

import java.util.Comparator;

public class TaskPriorityComparator implements Comparator<Task> {
    @Override
    public int compare(Task o1, Task o2) {
        return (int) (o1.getExecuteTime() - o2.getExecuteTime());
    }
}
