package DataGenerator;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class FileOperations {

    public void saveArray(ArrayList<Task> objArr) {
 
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;
 
        try {
 
            fout = new FileOutputStream("file.ser");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(objArr);
 
            System.out.println("Done");
 
        } catch (Exception ex) {
 
            ex.printStackTrace();
 
        } finally {
 
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
 
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
 
        }
    }

    public ArrayList<Task> loadArray() {
 
        ArrayList<Task> objArr = null;
 
        FileInputStream fin = null;
        ObjectInputStream ois = null;
 
        try {
 
            fin = new FileInputStream("file.ser");
            ois = new ObjectInputStream(fin);
            objArr = (ArrayList<Task>) ois.readObject();
 
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
 
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
 
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
 
        }
 
        return objArr;
 
    }
}