package assigner;

import DataGenerator.Task;
import assigner.algorithms.AssignerAlgorithmQueue;
import main.Main;

import java.util.ArrayList;

public class Assigner {
    AssignerAlgorithmQueue queue;

    public Assigner(AssignerAlgorithmQueue queue) {
        this.queue = queue;
    }

    public ArrayList<Task> getNextTasks(int n) {
        ArrayList<Task> newTasks = Main.generator.getNewTasks();
        newTasks.forEach(task -> task.log("in queue"));
        this.queue.putAll(newTasks);

        ArrayList<Task> tasks = queue.next(n);
        tasks.forEach(Task::setStarted);
        this.queue.getCollection().forEach(Task::incrementWaitingTime);

        return tasks;
    }

    public void acceptReturns(ArrayList<Task> returned) {
        this.queue.putAll(returned);
    }
}