package assigner.algorithms;

import DataGenerator.Task;
import javafx.scene.layout.Priority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.PriorityQueue;

public class SJF implements AssignerAlgorithmQueue {
    PriorityQueue<Task> queue = new PriorityQueue<>(Task.priorityComparator());

    @Override
    public void put(Task task) {
        queue.add(task);
    }

    @Override
    public void putAll(ArrayList<Task> task) {
        queue.addAll(task);
    }

    @Override
    public Task next() {
        return queue.poll();
    }

    @Override
    public boolean hasNext() { return queue.size() > 0; }

    @Override
    public Collection<Task> getCollection() {
        return this.queue;
    }
}
