package assigner.algorithms;

import DataGenerator.Task;

import java.util.ArrayList;
import java.util.Collection;

public interface AssignerAlgorithmQueue {
    void put(Task task);
    void putAll(ArrayList<Task> task);
    Task next();
    boolean hasNext();
    Collection<Task> getCollection();

    default ArrayList<Task> next(int count) {
        ArrayList<Task> tasks = new ArrayList<>();

        while (tasks.size() < count && this.hasNext()) {
            tasks.add(this.next());
        }

        return tasks;
    }
}
