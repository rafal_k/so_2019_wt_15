package assigner.algorithms;

import DataGenerator.Task;
import java.util.ArrayList;
import java.util.Collection;
import java.util.PriorityQueue;

public class FCFS implements AssignerAlgorithmQueue {
    ArrayList<Task> list = new ArrayList<>();

    @Override
    public void put(Task task) {
        list.add(task);
    }

    @Override
    public void putAll(ArrayList<Task> tasks) {
        list.addAll(tasks);
    }

    @Override
    public Task next() {
        return list.remove(0);
    }

    @Override
    public boolean hasNext() { return list.size() > 0; }

    @Override
    public Collection<Task> getCollection() {
        return this.list;
    }
}
