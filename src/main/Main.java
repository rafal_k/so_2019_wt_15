package main;

import CPU.CPUManager;
import assigner.Assigner;
import DataGenerator.Generator;
import assigner.algorithms.AssignerAlgorithmQueue;
import dataAnalysis.ConsoleDisplay;
import dataAnalysis.ResultContainer;
import dataAnalysis.ResultMock;
import log.output.ConsoleLoggerOutput;
import log.output.FileLoggerOutput;
import log.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static long time = 0;

    public static Generator generator;
    public static Assigner assigner;
    public static CPUManager cpuManager;

    public static Logger logger;

    static {
        try {
            logger = new Logger(new FileLoggerOutput("log.txt"));
        } catch (FileNotFoundException e) {
            logger = new Logger(new ConsoleLoggerOutput());
            logger.error("Log file not found. Falling back to console output.");
        }
    }

    public static void main(String[] args) {
        ArrayList<TestInstance> tests = Main.loadConfig("config.txt");

        List<ResultMock> results = new ArrayList<>();

        for (TestInstance instance : tests) {
            Main.cpuManager = new CPUManager(instance.getProcessorCount());

            Main.generator = new Generator(instance.getTaskCount());
            Main.generator.generateData();
            Main.generator.loadData();

            Main.assigner = new Assigner(instance.getAlgorithm());

            do {
                Main.cpuManager.tick();
                Main.time++;
            } while (!Main.generator.allTasksFinished());

            instance.setData(generator.getTasksArrayList());
            results.add(new ResultContainer(instance));
        }

        Main.display(results);
    }

    public static void display(List<ResultMock> results) {
        ConsoleDisplay.displayAllStats(results);
    }

    public static ArrayList<TestInstance> loadConfig(String filename) {
        ArrayList<TestInstance> instances = new ArrayList<>();

        try {
            Scanner in = new Scanner(Paths.get(filename));

            while (in.hasNextLine()) {
                String name = in.next();
                try {
                    Class<AssignerAlgorithmQueue> algorithmClass = (Class<AssignerAlgorithmQueue>) Class.forName("assigner.algorithms." + in.next());
                    AssignerAlgorithmQueue algorithm = algorithmClass.getConstructor().newInstance();

                    int processorCount = Integer.parseInt(in.next());
                    int taskCount = Integer.parseInt(in.next());

                    instances.add(new TestInstance(name, algorithm, processorCount, taskCount));
                } catch (ReflectiveOperationException e) {
                    System.out.println(String.format("Couldn't instantiate algorithm '%s'.", name));
                }
            }
        } catch (IOException e) {
            System.out.println("Couldn't read config file");
        }

        return instances;
    }

    public static long getTime() {
        return Main.time;
    }
}
