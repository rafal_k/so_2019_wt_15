package main;

import DataGenerator.Task;
import assigner.algorithms.AssignerAlgorithmQueue;

import java.util.ArrayList;

public class TestInstance {
    private String algorithmName;
    private AssignerAlgorithmQueue algorithm;
    private ArrayList<Task> tasks;
    private int processorCount;
    private int taskCount;

    public TestInstance(String name, AssignerAlgorithmQueue algorithm, int processorCount, int taskCount) {
        this.algorithm = algorithm;
        this.algorithmName = name;
        this.processorCount = processorCount;
        this.taskCount = taskCount;
    }

    public int getTaskCount() { return this.taskCount; }

    public void setData(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }

    public String getAlgorithmName() {
        return this.algorithmName;
    }

    public int getProcessorCount() {
        return this.processorCount;
    }

    public ArrayList<Task> tasks() {
        return this.tasks;
    }

    public AssignerAlgorithmQueue getAlgorithm() {
        return this.algorithm;
    }
}
