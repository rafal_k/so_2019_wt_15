package CPU;

import DataGenerator.Task;
import main.Main;

import java.util.ArrayList;

public class CPUManager{

     ArrayList<Processor> listCPU;


     public CPUManager(int numberOfProcessors){
        listCPU = new ArrayList<>();
        for(int i=0;i<numberOfProcessors;i++){
            listCPU.add(new Processor());
        }
     }

     public void tick(){
        accept(Main.assigner.getNextTasks(getAvailableCount()));
     }

     public void accept(ArrayList<Task> listTask){
         for(int i=0;i<listTask.size();i++){
             for(Processor proc : listCPU){
                 if(proc.getFree()){
                     proc.setTask(listTask.get(i));
                     break;
                 }
             }
         }
     }

     public int getAvailableCount(){
         int availableProcessors=0;
        for(Processor proc : listCPU){
            if(proc.getFree())
                availableProcessors++;
        }
        return availableProcessors;
     }



}