package CPU;
import DataGenerator.Task;
import main.Main;

public class Processor{

    private Task task;
    private long endTick;

    public Processor(){
        task = null;
        endTick=Main.getTime();
    }

    public boolean getFree(){
        if(task == null)
            return true;
        if(endTick == Main.getTime()) {
            task.setFinished();
            task = null;
            return true;
        }
        else
            return false;
    }

    public Task getTask(){
        return task;
    }

    public void setTask(Task task){

        this.task = task;
        endTick = Main.getTime() + task.getExecuteTime();
    }




}
