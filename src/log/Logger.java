package log;

import log.entry.ErrorLogEntry;
import log.entry.LogEntry;
import log.output.LoggerOutput;
import main.Main;

import java.util.ArrayList;

public class Logger {
    LoggerOutput output;
    ArrayList<LogEntry> entries = new ArrayList<>();

    public Logger(LoggerOutput output) {
        this.output = output;
    }

    public void log(String message) {
        LogEntry entry = new LogEntry(Main.getTime(), message);
        entries.add(entry);
        output.out(entry);
    }

    public void log(String prefix, String message) {
        LogEntry entry = new LogEntry(Main.getTime(), message, prefix);
        entries.add(entry);
        output.out(entry);
    }

    public void error(String message) {
        entries.add(new ErrorLogEntry(Main.getTime(), message));
    }
}
