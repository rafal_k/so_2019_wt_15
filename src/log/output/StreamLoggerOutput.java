package log.output;

import log.entry.LogEntry;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class StreamLoggerOutput implements LoggerOutput, AutoCloseable {
    OutputStream stream;
    PrintWriter writer;

    public StreamLoggerOutput(OutputStream stream) {
        this.stream = stream;
        this.writer = new PrintWriter(stream);
    }

    @Override
    public void out(ArrayList<LogEntry> entries) {
        entries.forEach(writer::println);
    }

    @Override
    public void close() throws RuntimeException {
        this.writer.close();
    }
}
