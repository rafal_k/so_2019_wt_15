package log.output;

import log.entry.LogEntry;

import java.util.ArrayList;

public class ConsoleLoggerOutput implements LoggerOutput {
    @Override
    public void out(ArrayList<LogEntry> entries) {
        entries.forEach(System.out::println);
    }
}
