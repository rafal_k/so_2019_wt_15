package log.output;

import log.entry.LogEntry;

import java.util.ArrayList;

public interface LoggerOutput {
    void out(ArrayList<LogEntry> entries);
    default void out(LogEntry entry) {
        ArrayList<LogEntry> list = new ArrayList<>(1);
        list.add(entry);
        this.out(list);
    }
}
