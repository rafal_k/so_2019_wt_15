package log.output;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class FileLoggerOutput extends StreamLoggerOutput {
    public FileLoggerOutput(String filename) throws FileNotFoundException {
        super(new FileOutputStream(filename));
    }
}
