package log.entry;

import org.apache.commons.lang3.StringUtils;

public class LogEntry {
    long time;
    String message;
    String prefix = "GENERAL";

    public LogEntry(long time, String message) {
        this.time = time;
        this.message = message;
    }

    public LogEntry(long time, String message, String prefix) {
        this(time, message);
        this.prefix = prefix;
    }

    public String toString() {
        return String.format("[%s] T%08d: %s", StringUtils.center(this.prefix, 10), this.time, this.message);
    }
}
