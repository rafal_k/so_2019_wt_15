package log.entry;

public class ErrorLogEntry extends LogEntry {
    public ErrorLogEntry(long time, String message) {
        super(time, message);
        this.prefix = "ERROR";
    }
}
