package dataAnalysis;

import java.util.List;

public class ConsoleDisplay {

    /**
     * Returns a double number rounded to two decimal places
     * @param number the double to be rounded
     * @return as above
     */
    private static double roundToTwoDec(double number){
        double x = (double) (Math.round(number * 100));
        x /= 100;
        return x;
    }

    /**
     * Prints in the console average waiting time of a process, formatted thus:
     * Nazwa algorytmu \t Sredni czas
     * alg1 \t avg1
     * alg2 \t avg2
     * ...
     * @param results a list of results, containing algorithm name and all waiting times of all processes
     */
    public static void displayAverageTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tSredni czas\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getAverage(element.getResultsArray()))  + '\n';
        }
        System.out.println(result);
    }

    /**
     * Prints in the console standard deviation of waiting time of a process, formatted thus:
     * Nazwa algorytmu \t Odchylenie stand.
     * alg1 \t std1
     * alg2 \t std2
     * ...
     * @param results a list of results, containing algorithm name and all waiting times of all processes
     */
    public static void displayStDevTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tOdchylenie stand.\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getStDev(element.getResultsArray())) + '\n';
        }
        System.out.println(result);
    }

    /**
     * Prints in the console the shortest waiting time of a process, formatted thus:
     * Nazwa algorytmu \t Najkrotszy czas
     * alg1 \t min1
     * alg2 \t min2
     * ...
     * @param results a list of results, containing algorithm name and all waiting times of all processes
     */
    public static void displayMinTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tNajkrotszy czas\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getMin(element.getResultsArray())) + '\n';
        }
        System.out.println(result);
    }

    /**
     * Prints in the console maximum waiting time of a process, formatted thus:
     * Nazwa algorytmu \t Najdluzszy czas
     * alg1 \t max1
     * alg2 \t max2
     * ...
     * @param results a list of results, containing algorithm name and all waiting times of all processes
     */
    public static void displayMaxTable(List<ResultMock> results){
        String result = "Nazwa algorytmu\tNajdluzszy czas\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getMax(element.getResultsArray()))  + '\n';
        }
        System.out.println(result);
    }

    /**
     * Prints in the console all statistics related to waiting time of a process, formatted thus:
     * Nazwa algorytmu \t Najdl. czas \t Najkr. czas \t Srednia \t Odch. stand.
     * alg1 \t max1 \t min1 \t avg1 \t std1
     * alg2 \t max2 \t min2 \t avg2 \t std2
     * ...
     * @param results a list of results, containing algorithm name and all waiting times of all processes
     */
    public static void displayAllStats(List<ResultMock> results){
        String result = "Nazwa algorytmu\tNajdl. czas\tNajkr. czas\tSrednia\tOdch. stand.\n";

        for (ResultMock element: results){
            result += element.getAlgorithmName() + '\t' + roundToTwoDec(Analysis.getMax(element.getResultsArray())) +
                    '\t' + roundToTwoDec(Analysis.getMin(element.getResultsArray())) + '\t' +
                    roundToTwoDec(Analysis.getAverage(element.getResultsArray())) + '\t' +
                    roundToTwoDec(Analysis.getStDev(element.getResultsArray())) + '\n';
        }

        System.out.println(result);

    }
}
