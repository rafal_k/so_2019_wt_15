package dataAnalysis;

public class Analysis {
	/**
	 * Returns an average of longs stored in an array
	 * @param data an array of longs to be used
	 * @return as above
	 */
	public static double getAverage (long[] data) {
		if (data.length == 0) return 0.0;

		double result = 0.0;

		for (long element : data)
			result += element;

		return result / data.length;
	}

	/**
	 * Returns standard deviation of longs stored in an array
	 * @param data an array of longs to be used
	 * @return as above
	 */
	public static double getStDev (long[] data) {
		if (data.length == 0) return 0.0;

		double result = 0.0;
		double average = getAverage(data);

		for (long element : data)
			result += Math.pow(element - average, 2);

		return Math.sqrt(result / data.length);
	}

	/**
	 * Returns the smallest item in an array of longs
	 * @param data the array of longs to be used
	 * @return as above
	 */
	public static long getMin (long[] data) {
		long min = Long.MAX_VALUE;

		for (long element : data) {
			min = element < min ? element : min;
		}

		return min;
	}

	/**
	 * Returns the largest item in an array of longs
	 * @param data the array of longs to be used
	 * @return as above
	 */
	public static long getMax (long[] data) {
		long max = Long.MIN_VALUE;

		for (long element : data)
			max = element > max ? element : max;

		return max;
	}
}
