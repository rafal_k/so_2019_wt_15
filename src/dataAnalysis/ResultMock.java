package dataAnalysis;

/**
 * Instances of the ResultMock interface can be used in the ConsoleDisplay static methods to present specific data
 * related to an array of all waiting times that are held by the instance of the ResultMock interface
 * @see ResultContainer
 */
public interface ResultMock {
    /**
     * Returns an array of waiting times of all processes that were used in the specific experiment
     * @return as above
     */
    long[] getResultsArray();

    /**
     * Returns the name of an algorithm that was used in the specific experiment
     * @return as above
     */
    String getAlgorithmName();
}
