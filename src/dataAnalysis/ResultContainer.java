package dataAnalysis;

import DataGenerator.Task;
import main.TestInstance;

import java.util.ArrayList;

/**
 * ResultContainer is a class whose instances can be used as a parameter of ConsoleDisplay static methods.
 *
 * The construction requires a TestInstance that contains all results related to a specific test, including
 * the name of the algorithm that was used in the experiment, and a list of Tasks containing the result waiting time.
 *
 * Both of the fields of TestInstance mentioned above are used to construct ResultContainer.
 * @see ResultMock
 */
public class ResultContainer implements ResultMock {

    private ArrayList<Long> times;
    private String algorithmName;


    /**
     * Creates an instance of ResultContainer that holds a specific algorithm name and waiting times of all tasks
     * @param testInstance a TestInstance that has an list of tasks and an algorithm name
     * @see ResultContainer
     */
    public ResultContainer(TestInstance testInstance){
        this.times = new ArrayList<Long>();

        for (Task element: testInstance.tasks()){
            times.add(element.getWaitingTime());
        }

        this.algorithmName = testInstance.getAlgorithmName();
    }

    /**
     * This method returns an array of waiting times of all processes that are in the TestInstance which was
     * used to construct the ResultContainer
     * @return as above
     * @see ResultContainer
     */
    @Override
    public long[] getResultsArray() {
        long[] result = new long[this.times.size()];

        for (int i = 0; i < result.length; i++){
            result[i] = times.get(i);
        }

        return result;
    }

    /**
     * This method returns the name of an algorithm that was used in a specific TestInstance which was used
     * to construct the ResultContainer
     * @return as above
     * @see ResultContainer
     */
    @Override
    public String getAlgorithmName() {
        return this.algorithmName;
    }
}
